﻿Imports System.Data.SqlClient
Public Class Connection
    Protected con As New SqlConnection
    Protected Function conected() 'function that creates the connection to the database
        Try
            'create connection
            con = New SqlConnection("data source=DESKTOP-GT6J0SL; initial catalog=vidcodesolutionsdb; integrated security=true;")
            'Open connection
            con.Open()
            Return True
        Catch e As Exception
            MsgBox(e.Message)
        End Try
    End Function

    'function that disconnects the program with the database.
    Protected Function disconnected()
        'Try...catch, for control errors during execution
        Try
            If con.State = ConnectionState.Open Then
                con.Close()
            End If
        Catch e As Exception
            MsgBox(e.Message)
        End Try
    End Function
End Class