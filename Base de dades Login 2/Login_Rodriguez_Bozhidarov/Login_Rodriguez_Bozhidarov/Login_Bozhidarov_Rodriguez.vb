Public Class Login_Bozhidarov_Rodriguez


    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Try
            Dim d As New Data
            Dim functions As New Func
            d.GetUser = Me.UsernameTextBox.Text
            d.GetPass = Me.PasswordTextBox.Text
            d.GType = Me.ComboBox1.SelectedItem

            If functions.Authenticate(d) = True Then
                'Dim frm As New Login_Bozhidarov_Rodriguez
                'frm.Show()
                Page2.Show()
                Me.Hide()



            Else
                MessageBox.Show("Error - Incorrect Data", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.UsernameTextBox.Clear()
                Me.PasswordTextBox.Clear()
                Me.UsernameTextBox.Focus()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        Close()
    End Sub

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub UsernameTextBox_Enter(sender As Object, e As EventArgs) Handles UsernameTextBox.Enter
        If UsernameTextBox.Text = "User" Then
            UsernameTextBox.ForeColor = Color.MidnightBlue
            UsernameTextBox.Text = ""
        End If
    End Sub

    Private Sub PasswordTextBox_Enter(sender As Object, e As EventArgs) Handles PasswordTextBox.Enter
        If PasswordTextBox.Text = "Password" Then
            PasswordTextBox.ForeColor = Color.MidnightBlue
            PasswordTextBox.Text = ""
            PasswordTextBox.UseSystemPasswordChar = True
        End If
    End Sub

    Private Sub UsernameTextBox_Leave(sender As Object, e As EventArgs) Handles UsernameTextBox.Leave
        If UsernameTextBox.Text = "" Then
            UsernameTextBox.Text = "User"
        End If
    End Sub

    Private Sub PasswordTextBox_Leave(sender As Object, e As EventArgs) Handles PasswordTextBox.Leave
        If PasswordTextBox.Text = "" Then
            PasswordTextBox.Text = "Password"
            PasswordTextBox.UseSystemPasswordChar = False

        End If
    End Sub

End Class
