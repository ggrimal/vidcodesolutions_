﻿Imports System.Data.SqlClient 'libreria
Public Class Func
    Inherits Connection 'create inheritance with the connection class
    Dim cmd As New SqlCommand


    Public Function Authenticate(ByVal d As Data) As Boolean
        Try
            Me.conected()
            cmd = New SqlCommand("authentication")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = con
            cmd.Parameters.AddWithValue("@user", d.GetUser)
            cmd.Parameters.AddWithValue("@pass", d.GetPass)
            cmd.Parameters.AddWithValue("@utype", d.GType)

            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader
            If dr.HasRows = True Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            Me.disconnected()
        End Try
    End Function
End Class