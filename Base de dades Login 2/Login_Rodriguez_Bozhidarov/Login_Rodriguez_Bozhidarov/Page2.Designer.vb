﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Page2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Page2))
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VidcodesolutionsdbDataSet = New Login_Rodriguez_Bozhidarov.vidcodesolutionsdbDataSet()
        Me.UsersTableAdapter = New Login_Rodriguez_Bozhidarov.vidcodesolutionsdbDataSetTableAdapters.usersTableAdapter()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TbLoggedAs = New System.Windows.Forms.TextBox()
        Me.LbLoggedAs = New System.Windows.Forms.Label()
        Me.btnLogOut = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabControlMain = New System.Windows.Forms.TabControl()
        Me.Tab_DirectDB_Access = New System.Windows.Forms.TabPage()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.panel6 = New System.Windows.Forms.Panel()
        Me.Users = New System.Windows.Forms.Label()
        Me.dgv_users = New System.Windows.Forms.DataGridView()
        Me.NameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PasswordDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.TabUsers = New System.Windows.Forms.TabPage()
        Me.TabDevices = New System.Windows.Forms.TabPage()
        Me.TabLocation = New System.Windows.Forms.TabPage()
        Me.TabSector = New System.Windows.Forms.TabPage()
        Me.TabRevision = New System.Windows.Forms.TabPage()
        Me.TabIncidence = New System.Windows.Forms.TabPage()
        Me.TabSuggestion = New System.Windows.Forms.TabPage()
        Me.TabPermission = New System.Windows.Forms.TabPage()
        Me.TabMaps = New System.Windows.Forms.TabPage()
        Me.ImageListMainMenu = New System.Windows.Forms.ImageList(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VidcodesolutionsdbDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.TabControlMain.SuspendLayout()
        Me.Tab_DirectDB_Access.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.panel6.SuspendLayout()
        CType(Me.dgv_users, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "users"
        Me.UsersBindingSource.DataSource = Me.VidcodesolutionsdbDataSet
        '
        'VidcodesolutionsdbDataSet
        '
        Me.VidcodesolutionsdbDataSet.DataSetName = "vidcodesolutionsdbDataSet"
        Me.VidcodesolutionsdbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'UsersTableAdapter
        '
        Me.UsersTableAdapter.ClearBeforeFill = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.TbLoggedAs)
        Me.Panel1.Controls.Add(Me.LbLoggedAs)
        Me.Panel1.Controls.Add(Me.btnLogOut)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.TabControlMain)
        Me.Panel1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.Panel1.Location = New System.Drawing.Point(15, 87)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(942, 537)
        Me.Panel1.TabIndex = 1
        '
        'TbLoggedAs
        '
        Me.TbLoggedAs.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.TbLoggedAs.Location = New System.Drawing.Point(112, 511)
        Me.TbLoggedAs.Name = "TbLoggedAs"
        Me.TbLoggedAs.Size = New System.Drawing.Size(103, 20)
        Me.TbLoggedAs.TabIndex = 16
        Me.TbLoggedAs.Tag = "LoggedAs"
        '
        'LbLoggedAs
        '
        Me.LbLoggedAs.AutoSize = True
        Me.LbLoggedAs.Font = New System.Drawing.Font("Roboto Lt", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbLoggedAs.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.LbLoggedAs.Location = New System.Drawing.Point(13, 513)
        Me.LbLoggedAs.Name = "LbLoggedAs"
        Me.LbLoggedAs.Size = New System.Drawing.Size(93, 15)
        Me.LbLoggedAs.TabIndex = 15
        Me.LbLoggedAs.Text = "Logged In As"
        '
        'btnLogOut
        '
        Me.btnLogOut.Font = New System.Drawing.Font("Roboto Lt", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogOut.Location = New System.Drawing.Point(807, 502)
        Me.btnLogOut.Name = "btnLogOut"
        Me.btnLogOut.Size = New System.Drawing.Size(103, 26)
        Me.btnLogOut.TabIndex = 14
        Me.btnLogOut.Text = "Log Out"
        Me.btnLogOut.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 476)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(0, 13)
        Me.Label1.TabIndex = 13
        '
        'TabControlMain
        '
        Me.TabControlMain.Controls.Add(Me.Tab_DirectDB_Access)
        Me.TabControlMain.Controls.Add(Me.TabUsers)
        Me.TabControlMain.Controls.Add(Me.TabDevices)
        Me.TabControlMain.Controls.Add(Me.TabLocation)
        Me.TabControlMain.Controls.Add(Me.TabSector)
        Me.TabControlMain.Controls.Add(Me.TabRevision)
        Me.TabControlMain.Controls.Add(Me.TabIncidence)
        Me.TabControlMain.Controls.Add(Me.TabSuggestion)
        Me.TabControlMain.Controls.Add(Me.TabPermission)
        Me.TabControlMain.Controls.Add(Me.TabMaps)
        Me.TabControlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlMain.Font = New System.Drawing.Font("Arimo", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControlMain.ImageList = Me.ImageListMainMenu
        Me.TabControlMain.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.TabControlMain.ItemSize = New System.Drawing.Size(10, 35)
        Me.TabControlMain.Location = New System.Drawing.Point(0, 0)
        Me.TabControlMain.Multiline = True
        Me.TabControlMain.Name = "TabControlMain"
        Me.TabControlMain.Padding = New System.Drawing.Point(10, 5)
        Me.TabControlMain.SelectedIndex = 0
        Me.TabControlMain.Size = New System.Drawing.Size(942, 537)
        Me.TabControlMain.TabIndex = 1
        '
        'Tab_DirectDB_Access
        '
        Me.Tab_DirectDB_Access.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Tab_DirectDB_Access.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Tab_DirectDB_Access.Controls.Add(Me.TabControl1)
        Me.Tab_DirectDB_Access.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Tab_DirectDB_Access.ImageKey = "006-database.png"
        Me.Tab_DirectDB_Access.Location = New System.Drawing.Point(4, 39)
        Me.Tab_DirectDB_Access.Name = "Tab_DirectDB_Access"
        Me.Tab_DirectDB_Access.Padding = New System.Windows.Forms.Padding(3)
        Me.Tab_DirectDB_Access.Size = New System.Drawing.Size(934, 494)
        Me.Tab_DirectDB_Access.TabIndex = 0
        Me.Tab_DirectDB_Access.Text = "DirectDB_Access"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage7)
        Me.TabControl1.Controls.Add(Me.TabPage8)
        Me.TabControl1.ItemSize = New System.Drawing.Size(71, 25)
        Me.TabControl1.Location = New System.Drawing.Point(25, 16)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(906, 401)
        Me.TabControl1.TabIndex = 12
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.MediumBlue
        Me.TabPage1.BackgroundImage = Global.Login_Rodriguez_Bozhidarov.My.Resources.Resources.bmw_logo_PNG19701
        Me.TabPage1.Controls.Add(Me.panel6)
        Me.TabPage1.Location = New System.Drawing.Point(4, 29)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(898, 368)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "DB_Users"
        '
        'panel6
        '
        Me.panel6.BackColor = System.Drawing.SystemColors.Highlight
        Me.panel6.Controls.Add(Me.Users)
        Me.panel6.Controls.Add(Me.dgv_users)
        Me.panel6.Location = New System.Drawing.Point(0, 0)
        Me.panel6.Name = "panel6"
        Me.panel6.Size = New System.Drawing.Size(899, 372)
        Me.panel6.TabIndex = 11
        '
        'Users
        '
        Me.Users.AutoSize = True
        Me.Users.Font = New System.Drawing.Font("Garamond", 20.0!, System.Drawing.FontStyle.Bold)
        Me.Users.ForeColor = System.Drawing.Color.White
        Me.Users.Location = New System.Drawing.Point(362, 17)
        Me.Users.Name = "Users"
        Me.Users.Size = New System.Drawing.Size(154, 30)
        Me.Users.TabIndex = 1
        Me.Users.Text = "Users in DB"
        '
        'dgv_users
        '
        Me.dgv_users.AutoGenerateColumns = False
        Me.dgv_users.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_users.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken
        Me.dgv_users.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_users.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NameDataGridViewTextBoxColumn, Me.PasswordDataGridViewTextBoxColumn, Me.TypeDataGridViewTextBoxColumn})
        Me.dgv_users.DataSource = Me.UsersBindingSource
        Me.dgv_users.Location = New System.Drawing.Point(40, 62)
        Me.dgv_users.Margin = New System.Windows.Forms.Padding(2)
        Me.dgv_users.Name = "dgv_users"
        Me.dgv_users.RowTemplate.Height = 24
        Me.dgv_users.Size = New System.Drawing.Size(679, 231)
        Me.dgv_users.TabIndex = 0
        '
        'NameDataGridViewTextBoxColumn
        '
        Me.NameDataGridViewTextBoxColumn.DataPropertyName = "name"
        Me.NameDataGridViewTextBoxColumn.HeaderText = "name"
        Me.NameDataGridViewTextBoxColumn.Name = "NameDataGridViewTextBoxColumn"
        '
        'PasswordDataGridViewTextBoxColumn
        '
        Me.PasswordDataGridViewTextBoxColumn.DataPropertyName = "password"
        Me.PasswordDataGridViewTextBoxColumn.HeaderText = "password"
        Me.PasswordDataGridViewTextBoxColumn.Name = "PasswordDataGridViewTextBoxColumn"
        '
        'TypeDataGridViewTextBoxColumn
        '
        Me.TypeDataGridViewTextBoxColumn.DataPropertyName = "type"
        Me.TypeDataGridViewTextBoxColumn.HeaderText = "type"
        Me.TypeDataGridViewTextBoxColumn.Name = "TypeDataGridViewTextBoxColumn"
        '
        'TabPage2
        '
        Me.TabPage2.Location = New System.Drawing.Point(4, 29)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(898, 368)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "DB_Devices"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Location = New System.Drawing.Point(4, 29)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(898, 368)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "DB_Location"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Location = New System.Drawing.Point(4, 29)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(898, 368)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "DB_Sector"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'TabPage5
        '
        Me.TabPage5.Location = New System.Drawing.Point(4, 29)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(898, 368)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "DB_Revision"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'TabPage6
        '
        Me.TabPage6.Location = New System.Drawing.Point(4, 29)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(898, 368)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "DB_Incidence"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'TabPage7
        '
        Me.TabPage7.Location = New System.Drawing.Point(4, 29)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Size = New System.Drawing.Size(898, 368)
        Me.TabPage7.TabIndex = 6
        Me.TabPage7.Text = "DB_Suggestion"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'TabPage8
        '
        Me.TabPage8.Location = New System.Drawing.Point(4, 29)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Size = New System.Drawing.Size(898, 368)
        Me.TabPage8.TabIndex = 7
        Me.TabPage8.Text = "DB_Permission"
        Me.TabPage8.UseVisualStyleBackColor = True
        '
        'TabUsers
        '
        Me.TabUsers.ImageKey = "008-user.png"
        Me.TabUsers.Location = New System.Drawing.Point(4, 39)
        Me.TabUsers.Name = "TabUsers"
        Me.TabUsers.Padding = New System.Windows.Forms.Padding(3)
        Me.TabUsers.Size = New System.Drawing.Size(934, 494)
        Me.TabUsers.TabIndex = 1
        Me.TabUsers.Text = "Users"
        Me.TabUsers.UseVisualStyleBackColor = True
        '
        'TabDevices
        '
        Me.TabDevices.ImageKey = "005-devices.png"
        Me.TabDevices.Location = New System.Drawing.Point(4, 39)
        Me.TabDevices.Name = "TabDevices"
        Me.TabDevices.Padding = New System.Windows.Forms.Padding(3)
        Me.TabDevices.Size = New System.Drawing.Size(934, 494)
        Me.TabDevices.TabIndex = 2
        Me.TabDevices.Text = "Devices"
        Me.TabDevices.UseVisualStyleBackColor = True
        '
        'TabLocation
        '
        Me.TabLocation.ImageIndex = 3
        Me.TabLocation.Location = New System.Drawing.Point(4, 39)
        Me.TabLocation.Name = "TabLocation"
        Me.TabLocation.Padding = New System.Windows.Forms.Padding(3)
        Me.TabLocation.Size = New System.Drawing.Size(934, 494)
        Me.TabLocation.TabIndex = 3
        Me.TabLocation.Text = "Location"
        Me.TabLocation.UseVisualStyleBackColor = True
        '
        'TabSector
        '
        Me.TabSector.Location = New System.Drawing.Point(4, 39)
        Me.TabSector.Name = "TabSector"
        Me.TabSector.Padding = New System.Windows.Forms.Padding(3)
        Me.TabSector.Size = New System.Drawing.Size(934, 494)
        Me.TabSector.TabIndex = 4
        Me.TabSector.Text = "Sector"
        Me.TabSector.UseVisualStyleBackColor = True
        '
        'TabRevision
        '
        Me.TabRevision.Location = New System.Drawing.Point(4, 39)
        Me.TabRevision.Name = "TabRevision"
        Me.TabRevision.Padding = New System.Windows.Forms.Padding(3)
        Me.TabRevision.Size = New System.Drawing.Size(934, 494)
        Me.TabRevision.TabIndex = 5
        Me.TabRevision.Text = "Revision"
        Me.TabRevision.UseVisualStyleBackColor = True
        '
        'TabIncidence
        '
        Me.TabIncidence.Location = New System.Drawing.Point(4, 39)
        Me.TabIncidence.Name = "TabIncidence"
        Me.TabIncidence.Padding = New System.Windows.Forms.Padding(3)
        Me.TabIncidence.Size = New System.Drawing.Size(934, 494)
        Me.TabIncidence.TabIndex = 6
        Me.TabIncidence.Text = "Incidence"
        Me.TabIncidence.UseVisualStyleBackColor = True
        '
        'TabSuggestion
        '
        Me.TabSuggestion.Location = New System.Drawing.Point(4, 39)
        Me.TabSuggestion.Name = "TabSuggestion"
        Me.TabSuggestion.Padding = New System.Windows.Forms.Padding(3)
        Me.TabSuggestion.Size = New System.Drawing.Size(934, 494)
        Me.TabSuggestion.TabIndex = 7
        Me.TabSuggestion.Text = "Suggestion"
        Me.TabSuggestion.UseVisualStyleBackColor = True
        '
        'TabPermission
        '
        Me.TabPermission.Location = New System.Drawing.Point(4, 39)
        Me.TabPermission.Name = "TabPermission"
        Me.TabPermission.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPermission.Size = New System.Drawing.Size(934, 494)
        Me.TabPermission.TabIndex = 8
        Me.TabPermission.Text = "Permission"
        Me.TabPermission.UseVisualStyleBackColor = True
        '
        'TabMaps
        '
        Me.TabMaps.Location = New System.Drawing.Point(4, 39)
        Me.TabMaps.Name = "TabMaps"
        Me.TabMaps.Padding = New System.Windows.Forms.Padding(3)
        Me.TabMaps.Size = New System.Drawing.Size(934, 494)
        Me.TabMaps.TabIndex = 9
        Me.TabMaps.Text = "Maps"
        Me.TabMaps.UseVisualStyleBackColor = True
        '
        'ImageListMainMenu
        '
        Me.ImageListMainMenu.ImageStream = CType(resources.GetObject("ImageListMainMenu.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListMainMenu.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageListMainMenu.Images.SetKeyName(0, "006-database.png")
        Me.ImageListMainMenu.Images.SetKeyName(1, "008-user.png")
        Me.ImageListMainMenu.Images.SetKeyName(2, "005-devices.png")
        Me.ImageListMainMenu.Images.SetKeyName(3, "004-placeholder.png")
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = Global.Login_Rodriguez_Bozhidarov.My.Resources.Resources.bmw_logo_PNG19701
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.Location = New System.Drawing.Point(914, -3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(87, 84)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Page2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.ClientSize = New System.Drawing.Size(1013, 636)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "Page2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Main Menu"
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VidcodesolutionsdbDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabControlMain.ResumeLayout(False)
        Me.Tab_DirectDB_Access.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.panel6.ResumeLayout(False)
        Me.panel6.PerformLayout()
        CType(Me.dgv_users, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents VidcodesolutionsdbDataSet As vidcodesolutionsdbDataSet
    Friend WithEvents UsersBindingSource As BindingSource
    Friend WithEvents UsersTableAdapter As vidcodesolutionsdbDataSetTableAdapters.usersTableAdapter
    Friend WithEvents Panel1 As Panel
    Friend WithEvents TabControlMain As TabControl
    Friend WithEvents Tab_DirectDB_Access As TabPage
    Friend WithEvents TabUsers As TabPage
    Friend WithEvents TabDevices As TabPage
    Friend WithEvents TabLocation As TabPage
    Friend WithEvents TabSector As TabPage
    Friend WithEvents TabRevision As TabPage
    Friend WithEvents TabIncidence As TabPage
    Friend WithEvents TabSuggestion As TabPage
    Friend WithEvents TabPermission As TabPage
    Friend WithEvents TabMaps As TabPage
    Friend WithEvents TbLoggedAs As TextBox
    Friend WithEvents LbLoggedAs As Label
    Friend WithEvents btnLogOut As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents TabPage6 As TabPage
    Friend WithEvents TabPage7 As TabPage
    Friend WithEvents TabPage8 As TabPage
    Private WithEvents panel6 As Panel
    Private WithEvents Users As Label
    Friend WithEvents dgv_users As DataGridView
    Friend WithEvents NameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PasswordDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TypeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents ImageListMainMenu As ImageList
End Class
