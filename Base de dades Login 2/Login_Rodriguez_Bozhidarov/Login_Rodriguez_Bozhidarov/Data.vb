﻿Public Class Data

    'class that contains the data.

    Dim user As String
    Dim pass As String
    Dim type As String

    Public Property GetUser()
        Get
            Return user
        End Get
        Set(ByVal value)
            user = value
        End Set
    End Property

    Public Property GetPass()
        Get
            Return pass
        End Get
        Set(ByVal value)
            pass = value
        End Set
    End Property
    'property that returns the user type.
    Public Property GType()
        Get
            Return type
        End Get
        Set(ByVal value)
            type = value
        End Set
    End Property
    'procedure that assigns the values depending on the parameter.
    Public Sub New(ByVal user As String, ByVal pass As String, ByVal type As String)
        Me.GetUser = user
        Me.GetPass = pass
        Me.GType = type
    End Sub

    Public Sub New()

    End Sub
End Class